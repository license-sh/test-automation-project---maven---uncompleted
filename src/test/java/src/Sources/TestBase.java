package src.Sources;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.PageObjectModel.Util.util.WebListeners;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

public class TestBase {
	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver event;
	public static WebListeners weblisteners;
	public static ATUTestRecorder recorder;
	
	public TestBase() throws IOException {
		prop = new Properties();
		FileInputStream filo = new FileInputStream("E:\\Study\\Automation Testing\\PageObjectModel\\src\\com\\PageObjectModel\\config\\Config.properties"); 
		prop.load(filo);
		String Url = prop.getProperty("URL");
//		String username = prop.getProperty("username");
//		String password = prop.getProperty("password");
	}
	public void Base(String browser) {
		if(browser.equalsIgnoreCase("chrome")) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/Resources/chromedriver.exe");
		driver = new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("IE")) {
		System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"/Resources/MicrosoftWebDriver.exe");
		driver = new InternetExplorerDriver();
		}
		event = new EventFiringWebDriver(driver);
		weblisteners = new WebListeners();
		event.register(weblisteners);
		driver = event ;
		driver.get(prop.getProperty("URL"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		WebElement Login = driver.findElement(By.xpath("//a[@href='https://ui.freecrm.com']"));
		Login.click();
		WebElement loginPageEmail = driver.findElement(By.xpath("//input[@placeholder='E-mail address']"));
		loginPageEmail.sendKeys(prop.getProperty("username"));
		WebElement loginPagePass = driver.findElement(By.xpath("//input[@placeholder='Password']"));
		loginPagePass.sendKeys(prop.getProperty("password"));	
		WebElement loginClick = driver.findElement(By.cssSelector("div[class='ui fluid large blue submit button']"));
		loginClick.click();
	}
	public void Video(String Video) throws ATUTestRecorderException {
		recorder = new ATUTestRecorder("E:\\Study\\Automation Testing\\PageObjectModel\\VideoRecorder",Video,false);
		recorder.start();
	}
	public void VideoEnd() throws ATUTestRecorderException {
//		recorder = new ATUTestRecorder("E:\\Study\\Automation Testing\\PageObjectModel\\VideoRecorder","TestRecord",false);
		recorder.stop();
	}

}
